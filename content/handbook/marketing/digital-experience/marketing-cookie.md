---

title: Marketing Cookies
description: >-
  Learn more about how Digital Experience uses browser cookies.
---

# Marketing cookie

The Digital Experience team uses the `gitlab_user` cookie as a tool to customize content on the `about.gitlab.com` domain.

This cookie determined if a user has an active sesssion within the GitLab product. No user information is passed to the cookie and it expires with the session. This implementation is on `gitlab.com` domain so it will propagate down to all subdomains.

For more information, read the [docs on that](https://docs.gitlab.com/ee/user/profile/#cookies-used-for-sign-in).

## Related MRs

* Updating marketing cookie: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/131072
* Setting marketing cookie: for logged in users: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113761


 <figure class="video_container">
   <iframe src="https://www.youtube.com/embed/Nm8wWtoBCTc" frameborder="0" allowfullscreen="true"> </iframe>
 </figure>
